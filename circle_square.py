from math import pi


def circle_square(radius=None, diameter=None) -> float:
    """
    Function to calculate the circle square with given radius or diameter.
    If there was given value explicitly not signet to one of the arguments, it will be considered as a radius value.

    Parameters
    ----------
    radius [optional] value of the radius of the circle
    diameter [optional] value of the diameter of the circle

    Returns
    -------
    value of area of a circle: float
    """
    if radius is not None:
        s = pi * radius ** 2
    else:
        s = (pi * diameter ** 2) / 4

    return s.__round__(2)


print(circle_square(radius=10))
print(circle_square(radius=2))
print(circle_square(diameter=30))
print(circle_square(diameter=50))
